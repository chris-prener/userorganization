Dear Yves, Mine, Ludwig, Norman, Isabella and Uwe,

Please find below the schedule for your talks in the useR! 2017 conference.

We will send you the details about the exact time of your lecture later on.

Can you please send us the final title of your talk and a short abstract so we can upload this to the website (http://www.user2017.brussels/)?

If you have any question or request, please let us know.

Looking forward to see you in Brussels, best regards, ziv and heather.



## schedule for invited speakers ###

Wed 5 July

Yves Roseel
Mine Cetinkaya-Rundel

Thur 6 July

Ludwig Hothorn
Norman Matloff

Fri 7 July 

Isabella Gollini
Uwe Ligges