Dear _____

We are pleased to announce that the next International R User Conference, useR! 2017, will take place in Brussels, Belgium on July 4-7, 2017. We expect around 1000-1200 participants in the conference.

Given your expertise in __________ we would like to invite you to give a keynote lecture at useR! 2017 on a subject of your choice. Our aim is to have the invited lectures cover a broad spectrum of topics ranging from technical and R-related computing issues to general statistical topics of current interest. The length of each keynote lecture is expected to be approximately 45 minutes. 

If you decide to accept this invitation, we will waive the registration fee for the conference and reimburse your travel and accommodation costs.

Further information about the previous conference can be found on http://user2016.org/.  A link to the website of useR! 2017 will be available soon.

We thank you for considering this invitation, and hope that you might be able to contribute to the conference.

Should you have any questions, please do not hesitate to contact us.

Looking forward to hear from you.

Best wishes,

Ziv Shkedy and Heather Turner,Chairs of the program committee.