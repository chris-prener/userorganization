Dear All,

The deadline for tutorial submission has now passed. We received many more proposals than expected, 51 altogether! Therefore, rather than asking everyone on the committee to review all proposals, we have split them into two groups and assigned 9 reviewers to each group:

Group 1, statistical modelling and methods: Mette, Roger, Szilard, Pierre, Julia, Andrea, Michela, Thibault, Éric
Group 2, everything else: Heather, Yihui, Nicolas, Matthias, Martyn, Jan, Diane, Toby, Christine

Please submit your reviews via https://user2019.sciencesconf.org. We will be using this to review talks and posters as well, so it's good to get familiar with it now! You'll find your allocated reviews under `Reviewing > New submissions`. Please score each abstract from 0 to 4, guidelines as follows:

0 - bottom of the pile
1 - okay, but not inspiring
2 - middle of the road
3 - solid choice
4 - top notch, really hope this one is picked

The local organisers in collaboration with the R Foundation Conference Committee have already invited 9 tutorials: http://www.user2019.fr/tutorials/. The total number of tutorials is not finalised but you should expect only around 4 tutorials from your group to make the cut, so bear that in mind when scoring. Your score should reflect the quality of the proposal and the suitability for useR! (i.e. will it contribute to a balanced program of distinctive content for the participants we expect at useR!). 

Note the submission guidelines were to submit an abstract <1200 characters without any attachment, so any attachments should be regarded as supplementary information.

You can add a short ("Internal") comment to explain your score, if you wish. You can ignore everything else on the form. You can edit your reviews via `Reviewing > Submissions reviewed`. If you're not
comfortable scoring a particular abstract, you can leave it unreviewed, but please send us an email to let us know you've reviewed as many abstracts in your group as you can.

To help with your reviews, we attach combined HTMLs of the abstracts in each group and a csv of the data for all abstracts.

Please complete your reviews by Wednesday Feb 6th. We will collate the ratings and let you know if we need further assistance finalising the selection (authors will be notified on Feb 15th).

Many thanks!

Heather and Pierre