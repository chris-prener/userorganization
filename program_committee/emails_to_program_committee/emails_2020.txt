Follow-up email upon acceptance
 
Subject: Information to Give You Credit for Contributing to useR! 2021 Program Committee
 
Hi,
 
Thank you very much for accepting to be part of useR! 2021 Program Committee.
 
Please complete this short form (link to LimeSurvey) with your information by (seven days from sending this email) so we can publicly acknowledge your work to useR! 2021 on our website and social media.
 
We will be reaching out again nearer the review period begins with additional information about the review process.
 
Best,
 
Laura Ación, Yanina Bellini Saibene, Rocío Joo, Dorothea Hug Peter, and Heather Turner
Chairs of the Program Committee
 
 
 
Email to folks reviewing tutorials
 
Subject: Invitation to Join UseR! 2021 Program Committee
 
Hi,
 
We are pleased to announce that useR! 2021, the next International R User Conference, will take place online on July 5-9, 2021.
  
Given your expertise in R, we would like to invite you to be a member of the useR! 2021 Program Committee. This will greatly help us to consolidate the preferences for the contributions that will be presented during useR! 2021.
 
If you accept this invitation, we would like you to review some one-page tutorial proposals. At this time it is not possible to anticipate how many proposals we will assign to you. However, we will do our best to keep your service as a program committee member to a maximum of eight hours. 
 
We expect that your service will approximately take place during the last three weeks of February 2021, once the tutorial submission process has been completed.
 
We thank you for considering this invitation and hope that you might be able to contribute to the conference.  
 
Should you have any questions, please do not hesitate to contact us. 
 
Looking forward to hearing from you at your earliest convenience, before October 19th, 2021.
 
Best wishes, 
 
Laura Ación, Yanina Bellini Saibene, Dorothea Hug Peter, Rocío Joo, and Heather Turner
Chairs of the Program Committee
 
Email to folks reviewing abstracts
 
Subject: Invitation to Join UseR! 2021 Program Committee
 
Hi,
 
We are pleased to announce that useR! 2021, the next International R User Conference, will take place online, on July 5-9, 2021.
  
Given your expertise in R, we would like to invite you to be a member of the useR! 2021 Program Committee. This will greatly help us to consolidate the preferences for the contributions that will be presented during useR! 2021.
 
If you accept this invitation, we would like you to review some abstracts. At this time it is not possible to anticipate how many abstracts we will assign to you. However, we will do our best to keep your service as a program committee member to a maximum of eight hours. 
 
We expect that your service will approximately take place during the last three weeks of March 2021, once the abstract submission process has been completed.
 
We thank you for considering this invitation and hope that you might be able to contribute to the conference.  
 
Should you have any questions, please do not hesitate to contact us. 
 
Looking forward to hearing from you at your earliest convenience, before October 19th, 2021.
 
Best wishes, 
 
Laura Ación, Yanina Bellini Saibene, Rocío Joo, Dorothea Hug Peter, and Heather Turner
Chairs of the Program Committee
 
 
 
