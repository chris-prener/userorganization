Dear ______,

We are pleased to announce that the next International R User Conference, useR! 2017,  will take place in Brussels, Belgium on July 4-7,2017. We expect around 1000 participants in the conference.
  
Given your expertise in R we would like to invite you to be a member of the program committee of the conference.
  
We thank you for considering this invitation, and hope that you might be able to contribute to the conference.  
 
Should you have any questions, please do not hesitate to contact us.
 
Looking forward to hear from you.
 
Best wishes, 

Ziv Shkedy and Heather Turner, Chairs of the program committee.