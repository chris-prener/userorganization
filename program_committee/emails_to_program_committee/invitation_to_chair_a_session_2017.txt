Dear R Foundation members, Program Committee and Tutors,

We would like to invite you to chair a session at useR! 2017. Please let us know if you are willing to do this and take a look at the online schedule, https://user2017.sched.com/grid-full/#2017-07-05, to identify any particular session(s) you would like to chair. (The session name can be seen by hovering over the talk title, or by clicking a talk to see the description). Also let us know if you will only attend on certain days.

**Please let us know this by Sunday 19 June**

R Foundation members, please note there are still spaces available at useR! (although nearly 900 have signed up now) so if you're thinking of coming but have not yet registered, please do so!

Best wishes,

Heather and Ziv
Co-chairs useR! 2017 Program Committee