# useR! abstract submission: guidance for chairs

## Before reviewing starts

### Delegating abstracts to reviewers

The best way to delegate abstracts to reviewers may depend on the reviewing system used. As an indication, for useR! 2019, submitters were asked to select one topic for their abstracts. This topic was reviewed by the chairs for all abstracts, since several submitters selected more than one topic (the form could not be restricted), selected an unsuitable topic by mistake (it was a drop-down form) or selected "other". Previously two reviewers had been allocated to each topic, according to their expertise/preferences. Due to the imbalance between topics, some topics were re-distributed among the reviewers and some abstracts were reallocated so that in the end each reviewer had 45-55 abstracts each across 2 or 3 topics.

Check that reviewers aren't assigned their own abstracts to review.

Reviewers had 4 weeks to return their reviews. It is a good idea to ask reviewers to confirm (people may have changed email address since joining committee).

### Duplicated presenters

If possible identify duplicated presenters and highlight these to reviewers.

## Once reviews are in

### Duplicated presenters

Review these scores/comments first aiming to reduce to at most one accept/maybe per presenter.

As you go through the remaining abstracts, you may notice several abstracts by the same team; ideally a reasonable limit will be placed on the team, e.g. an oral presentation and a poster for a team of two.

### Duplicated work

You should review the schedule of the last useR! and any nearby R conferences in the past year to check for duplicated presentations. Preference should be given to abstracts presenting new material, exceptions may be given for very significant/original work.

### First pass through remaining abstracts

As a first pass, abstracts with a mean score of >7 can be accepted. Review these to double check, e.g. if the code is not (yet) freely available the abstract may need to be rejected for this year. Review any recommended changes of formats for these abstracts to set the format for these talks. This gives an idea of the numbers in each format and the space left. It's a good idea to add comments as you go through, for the co-chair and in case the abstracts need revisiting.

### Second pass through remaining abstracts

It is good to review the remaining abstracts by topic, so you can see if there are topics where people have been more generous/more harsh, or if you already have similar presentations accepted, etc.

If you have a co-chair, this task can be split, it is not necessary to double-check each others' decisions as you will mainly be selecting lightning and posters now. Of course, some abstracts can be left as "maybe" if you want a second opionion and it is good to add comments as you go through. Also you can split the abstracts so you are not making decisions on any abstracts you previously reviewed. You can each aim to select a set number in each format, to meet the targets.

It is possible that abstracts reviewed in this stage may be better than ones accepted in the first pass (e.g. due to some reviewers being harsher than others). You will have a better overview than the reviewers after reviewing so many abstracts, so use your judgement to select the best abstracts overall rather be bound by the score.

## Final decisions

It is good to go over any abstracts downgraded from accept and any accepted abstracts with low scores (mean < 5.5). Also any that require a second opinion or discussion, e.g. abstracts from the same team or on very similar topic that were considered separately in the last stage.

## General principles

### Over/under-accept?

It is not necessary to fill the schedule; in particular, it is common for fewer oral presentations to be accepted than the maximum possible, to ensure high quality presentations. On the other hand, drop-out tends to be higher for lightning talks and posters, so you are recommended to accept more than the ideal number. In the unlikely event drop-out is much lower than previous years, lightning talks and posters are easier to squeeze in.

Drop out 2019:

talk: 7/141 ~ 5%
lightning: 19/82 ~ 23%
poster: 24/110 ~22%

### Niche topics

In general, useR! accepts abstracts from all domains, however very niche case studies may not be given the benefit of the doubt if they are not mature enough/do not give enough detail on the methods or R packages used/do not have any code available yet.

### Demographics

In general, selection should be based on the merit of the abstract. However, some account of demographics may be taken for the final selection of borderline cases, particularly for lightning talks and posters. E.g. benefit of the doubt may be given to 

 - local presenters (who may only come to this useR!)
 - sponsors
 - diversity scholars (only chairs should be informed who has been awarded diversity scholarships)
 - under-represented groups (as far as can be judged from available information, e.g. gender and country of residence may be given in abstract submission)
 
Feedback may be given in some of these cases to help improve the presentation.