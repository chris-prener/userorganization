# useR! abstract submission: guidance for reviewers

The submission numbers are as follows

talk: [x1]
lightning talk: [y1]
poster: [z1]

and we have planned for

talk: [x2]
lightning talk: [y2]
poster: [z2]

So we expect to reject around 1 in [N] abstracts, reallocating abstracts to different formats to achieve a strong program overall.

Your review consists of an overall score, a brief internal comment and optionally brief comments to the authors, a recommendation for allocation to a different format, and recommendations for scheduling.

## Overall Score

This guides the overall accept/reject decision the program chairs will make after receiving your reviews:

0 - Faux-pas (reject)
4 - Clich� (possibly reject)
7 - Connoisseur (probably accept)
10 - Tour de force (definitely accept)

Please score the abstract for the format you recommend. For example, you may think an abstract is not good enough/suited to a regular talk, but is fine as a lightning talk. Then you should score it as 7 and recommend to reallocate as a lightning talk.

Try to use the whole range of scores to help differentiate abstracts. A zero does not necessarily mean an abstract is totally without merit, just that it is not good enough relative to other submissions or fails to meet the requirements for useR! (discussed further below).

As a first pass, chairs will accept abstracts with at least two 7s.

## Internal Comment

A sentence explaining your score is very helpful to the chairs when making final decisions.

## External Comment (depending on review system)

An optional comment which may be relayed to the contributor if they ask for feedback [or shown automatically to authors depending on review system].

## Recommended Format

You may recommend a change from the authors' preferred format. Choose according to the scope and quality of the submission:

 - oral presentation: novel, mature, substantial contributions
     - code/methods must be available (at least on GitHub or similar)
 - lightning talk: fun/short topics, accessible case studies
     - no live coding or interactive graphics
 - poster: detailed case studies, works-in-progress, projects for discussion/feedback
     - no interactivity
 
"Upgrades" (poster -> lightning talk -> talk) should be rare (~5%) while "downgrades" should be fairly common (~15%).

For abstracts you score as zero, give the format best suited if the abstract is accepted (the other reviewer/the chairs may see a reason to give it a chance).

## Recommended scheduling

Comments on a suggested topic for the session, or abstracts that pair well together are welcome.

## Miscellaneous

### Authorship

- If there is a conflict of interest, e.g. you have been assigned an abstract on which you are a co-author or close colleague, please ask for the abstract to be reassigned.

- Please look out for multiple abstracts from the same author/set of authors. Only one abstract will be accepted per presenter (exceptions made for presenters representing a community/organization). Comments on your preferred abstract are weclome.

### Novelty

- If you are aware that the work described in the abstract has been presented elsewhere, please comment on this and note if there is any new material in the current abstract.

- Comments on you preferred abstract among very similar submissions are welcome.

- Tutorial-style presentations on other peoples' packages are not acceptable. Presentations should contribute something new, at least a substantive review.

### Reproducibility

- Give preference to abstracts where the source data/methods/code/results are 
published in some form, enabling reproducibility/transferability. This is essential for oral presentations, even if the abstract otherwise appears good.

- It is essential for oral presentations that the approach can be replicated for free, at least in basic form (e.g. a free community version).

- Checking people's webpages/GitHub is encouraged if the decision is not clear-cut.