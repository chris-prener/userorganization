# Why use captioning?

Captioning helps all participants to follow what is being said, but is particularly help for participants that are Deaf/hard-of-hearing, are not fluent in English, or find certain accents hard to understand. See e.g. [this medium post](https://medium.com/@lazerwalker/your-online-event-should-have-live-captions-87f332a35745) about captions at conferences.

# What types of captioning are there?

 - Live captioning by a stenographer: slightly delayed but reasonably accurate;
 - Live auto-captioning (e.g. as implemented in Google Slides, Powerpoint, or tools such as [web captioner](https://webcaptioner.com/) or https://github.com/MidCamp/live-captioning): in sync but often inaccurate;
 - Closed captions added to a video: in sync, quality will depend whether caption are created using a transcript, using professionally-edited auto-captions or auto-captions.
 
You can see live captions by a stenographer, closed auto-captions by You-Tube and closed captions based on professionally-edited auto-captions on [useR! 2020 keynote videos](https://www.youtube.com/playlist?list=PL4IzsxWztPdltBEpbd5B0jLJS13xAyq5n). 

# Isn't auto-captioning good enough?

Auto-captioning is a free/cheap option, but not good enough for professional, technical events. This is a consistent message from disability advocates (e.g. [US National Deaf Center](https://www.nationaldeafcenter.org/covid19faqs)), specialists ( e.g. [accessibility team at the University of Melbourne](https://www.unimelb.edu.au/accessibility/tutorials/zoom/zoom-meetings-and-closed-captions)) and the Deaf community (e.g. [on The Mind Hears blog](https://themindhears.org/2019/05/01/captions-and-craptions-for-academics/)). In the context of R, auto-captioning will often get words such as R, RStudio, package names and other domain-specific language incorrect, which quickly becomes irritating and it's easy to miss critical information. If a conference/session is auto-captioned live Deaf folk are more likely to avoid attending and wait for the recorded video to get a better experience (assuming it is re-captioned afterwards), so it does not provide equitable access.

# What about sign language?

For some people, sign language is their first language and they wouldn't attend an event that didn't provide it. However for an international conference it is difficult as there are many dialects - even sometimes within a country (e.g. Black American Sign Language is different from American Sign Language). For an in-person conference, sign language for the dominant local dialect might be considered. Or a budget might be put aside to respond to accommodation requests, where appropriate provision could be matched to the partipant's requirements.